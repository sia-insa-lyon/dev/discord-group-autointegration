# Configurateur automatique de l'intégration discord
Si tu es un mec stylé et que tu utilise des Webhooks avec tous tes services tu as sûrement eu l'idée de de configurer des Webhooks pour
recevoir les notifications Gitlab dans Discord. Mais vu qu'on a 50000 repos Gitlab, tu t'es sûrement dit: "Je vais utiliser
un webhook de groupe !".

Sauf que ça ne marche pas, car Discord a besoin d'une syntaxe spéciale, qui est disponible dans les *Integrations*, mais cet onglet
n'est disponible que dans un projet, et pas à l'échelle d'un groupe entier.
Donc il faut setup le webhook Discord dans chaque projet un par un. Et c'est long.

Du coup ce client d'API Gitlab & Discord fait tout cela à ta place. 
## Préparation
Créez un bot Discord et ajoutez-le au serveur qui va bien, et créez un Personal Access Token (voir usage CLI)
```
https://discord.com/oauth2/authorize?client_id=client_id_de_votre_application&permissions=536870912&scope=bot
```
# Utilisation
## En ligne de commande

```
Usage of ./main:
  -bot-name string
        Nom du bot Gitlab (default "Gitlab autoBot")
  -channel-id string
        L'ID du channel Discord qui sera utilisé par le bot Gilab. Activez les outils développeur et faites clic droit sur un channel puis 'Copier l'identifiant' (default "UNSET")
  -discord-token string
        Créez un bot discord et récupérez son token: Créer une 'Application' ici https://discord.com/developers/applications et allez dans Bot. (default "UNSET")
  -gitlab-pat string
        Créez-le avec le scope 'api' ici: https://gitlab.com/profile/personal_access_tokens (default "UNSET")
  -group-id int
        L'ID du groupe Gitlab à utiliser
  -add-mode booléen: si false, ça supprime les webhooks de gitlab au lieu de les configurer
```

## Avec variable d'environnement
Il faut lancer le script ``use_env.sh``
```
CHANNEL_ID=
BOT_NAME=
GITLAB_PAT=
DISCORD_TOKEN=
GROUP_ID=
ADD_MODE=true
```

## Configuration Gitlab
La configuration des webhooks peut être changée en éditant la fonction

```go
func addDiscordWebHookToProject(gl *gitlab.Client, pid int, webhookUrl string) error {
	all := "all"
	_, err := gl.Services.SetDiscordNotificationService(pid, &gitlab.SetDiscordNotificationServiceOptions{
		WebHook:                   &webhookUrl,
		NotifyOnlyBrokenPipelines: &oui,
		NotifyOnlyDefaultBranch:   &non,
		BranchesToBeNotified:      &all,
		ConfidentialIssuesEvents:  &oui,
		ConfidentialNoteEvents:    &oui,
		DeploymentEvents:          &non,
		IssuesEvents:              &oui,
		MergeRequestsEvents:       &non,
		TagPushEvents:             &non,
		NoteEvents:                &oui,
		PipelineEvents:            &oui,
		PushEvents:                &non,
		WikiPageEvents:            &non,
	})
	return err
}
```