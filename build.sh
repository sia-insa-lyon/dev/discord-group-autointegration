#!/bin/bash
set -e
set -x
go mod tidy
go mod vendor
cat gitlab-discord-service-patch.txt >> vendor/github.com/xanzy/go-gitlab/services.go
go build main.go