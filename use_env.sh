#!/bin/bash
if [[ -e .env ]];then
  source ./.env
fi
./main  -bot-name "${BOT_NAME}" \
  -channel-id "${CHANNEL_ID}" \
  -discord-token "${DISCORD_TOKEN}" \
  -gitlab-pat "${GITLAB_PAT}" \
  -group-id "${GROUP_ID}" -add-mode "${ADD_MODE}"
