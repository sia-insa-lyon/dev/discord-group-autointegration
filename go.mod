module discord-gitlab

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/xanzy/go-gitlab v0.38.1
)
