package main

import (
	"flag"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/xanzy/go-gitlab"
	"log"
	"os"
)

func recurse(gid int, gl *gitlab.Client) []*gitlab.Project {
	subgroups, _, gsgerr := gl.Groups.ListSubgroups(gid, &gitlab.ListSubgroupsOptions{})
	he(gsgerr)
	projects, _, spgerr := gl.Groups.ListGroupProjects(gid, &gitlab.ListGroupProjectsOptions{})
	he(spgerr)
	for _, subgroup := range subgroups {
		log.Printf("Fetching subgroups and projects of group %s", subgroup.FullPath)
		projects = append(projects, recurse(subgroup.ID, gl)...)
	}
	return projects
}

func main() {
	var botChannelId string
	var botName string
	var gitlabToken string
	var discordToken string
	var addMode bool
	botAvatar := ""
	gitlabGroupId := 0
	flag.StringVar(&botChannelId, "channel-id", "UNSET", "L'ID du channel Discord qui sera utilisé par le bot Gilab. Activez les outils développeur et faites clic droit sur un channel puis 'Copier l'identifiant'")
	flag.StringVar(&botName, "bot-name", "Gitlab autoBot", "Nom du bot Gitlab")
	flag.StringVar(&gitlabToken, "gitlab-pat", "UNSET", "Créez-le avec le scope 'api' ici: https://gitlab.com/profile/personal_access_tokens")
	flag.StringVar(&discordToken, "discord-token", "UNSET", "Créez un bot discord et récupérez son token: Créer une 'Application' ici https://discord.com/developers/applications et allez dans Bot.")
	flag.IntVar(&gitlabGroupId, "group-id", 0, "L'ID du groupe Gitlab à utiliser")
	flag.BoolVar(&addMode, "add-mode", true, "Si false, ça supprime au lieu d'ajouter un webhook")
	flag.Parse()

	if botChannelId == "UNSET" || gitlabToken == "UNSET" || discordToken == "UNSET" || gitlabGroupId == 0 {
		println("Mauvaise configuration, des arguments sont manquants. Retournez voir le README")
		os.Exit(1)
	}

	gl, err := gitlab.NewClient(gitlabToken)
	he(err)

	sess, err := discordgo.New("Bot " + discordToken)
	if err != nil {
		fmt.Print("error creating Discord session,", err)
		panic(err)
	}

	if addMode { //add webhook
		existingwebhooks, ewerr := sess.ChannelWebhooks(botChannelId)
		he(ewerr)
		var webhook *discordgo.Webhook
		webhook = nil
		for _, wh := range existingwebhooks { //récup un webhook existant
			if wh.Name == botName {
				webhook = wh
				log.Print("Found existing Discord Webhook, reusing")
			}
		}
		if webhook == nil { //crée un nouveau
			log.Print("Creating new Discord webhook")
			wh, whcerr := sess.WebhookCreate(botChannelId, botName, botAvatar)
			he(whcerr)
			webhook = wh
		}

		webhoolurl := getWebhookUrl(webhook)
		log.Printf("Fetching all projects from group #%d", gitlabGroupId)
		projects := recurse(gitlabGroupId, gl)
		log.Print("Fetched all projects from all subgroups")
		l := len(projects)
		for i, proj := range projects {
			log.Printf("[%d/%d] Applying webhook to project %s", i, l, proj.NameWithNamespace)
			he(addDiscordWebHookToProject(gl, proj.ID, webhoolurl))
		}
		log.Print("Succeeded")
	} else { // remove webhooks
		projects := recurse(gitlabGroupId, gl)
		log.Print("Fetched all projects from all subgroups")
		l := len(projects)
		for i, proj := range projects {
			log.Printf("[%d/%d] Removing webhook from project %s", i+1, l, proj.NameWithNamespace)
			he(removeDiscordIntegration(gl, proj.ID))
		}
	}

	os.Exit(0)
}

func he(err error) {
	if err != nil {
		panic(err)
	}
}
func addDiscordWebHookToProject(gl *gitlab.Client, pid int, webhookUrl string) error {
	oui := true
	non := false
	all := "all"
	_, err := gl.Services.SetDiscordNotificationService(pid, &gitlab.SetDiscordNotificationServiceOptions{
		WebHook:                   &webhookUrl,
		NotifyOnlyBrokenPipelines: &oui,
		NotifyOnlyDefaultBranch:   &non,
		BranchesToBeNotified:      &all,
		ConfidentialIssuesEvents:  &oui,
		ConfidentialNoteEvents:    &oui,
		DeploymentEvents:          &non,
		IssuesEvents:              &oui,
		MergeRequestsEvents:       &non,
		TagPushEvents:             &non,
		NoteEvents:                &oui,
		PipelineEvents:            &oui,
		PushEvents:                &non,
		WikiPageEvents:            &non,
	})
	return err
}
func removeDiscordIntegration(gl *gitlab.Client, pid int) error {
	_, err := gl.Services.DeleteCustomIssueTrackerService(pid)
	return err
}
func getWebhookUrl(webhook *discordgo.Webhook) string {
	return "https://discordapp.com/api/webhooks/" + webhook.ID + "/" + webhook.Token
}
